Definition
forEach() calls a provided callback function once for each element in an array in ascending order.
 It is not invoked for index properties that have been deleted or are uninitialized.

callback is invoked with three arguments:

the value of the element
the index of the element
the Array object being traversed

If a thisArg parameter is provided to forEach(), it will be used as callback's this value.
The thisArg value ultimately observable by callback is determined according to the usual rules for determining the this seen by a function.
The forEach() method calls a function once for each element in an array, in order.
the function is not executed for array elements without values.

Syntax
array.forEach(function(currentValue, index, arr), thisValue)


Example

let sum = 0;
let numbers = [65, 44, 12, 4];
numbers.forEach(myFunction);

function mFunction(item) {
  sum += item;
  document.getElementById("demo").innerHTML = sum;
}
